# jenkins-ansible-examples

#### 介绍
基于jenkins执行ansible任务实例集合
- [基于ansible远程docker环境安装gogs ](https://ost.51cto.com/posts/13078)
- [基于ansible远程docker环境安装halo博客系统](https://ost.51cto.com/posts/12819)
- [基于ansible远程docker环境安装redis集群 ](https://ost.51cto.com/posts/13471)
- [基于ansible远程docker环境安装kafka集群 ](https://ost.51cto.com/posts/13977)

结合 https://gitee.com/pi4k8s/ansible-exmaples.git 对centos目标机进行安装。
