def ansible = docker.image('pi4k8s/ansible:centos7')
node {

    stage('git chekout') {
        parallel(
                'common': {
                    dir('common'){
                        git branch: 'master', url: 'https://gitee.com/pi4k8s/ansible-exmaples.git '
                    }
                },
                'halo': {
                    dir('redis-cluster'){
                        git branch: 'master', url: 'https://gitee.com/cuiyingfeng/jenkins-ansible-examples.git'
                    }

                }
        )
    }
    stage('aggregate') {
        sh 'rm -rf aggregate && mkdir aggregate'
        sh 'cp -r ./redis-cluster/redis-cluster ./aggregate'
        sh 'cp -r ./common/base-centos7.9/playbook/roles/os-enforce ./aggregate/redis-cluster/playbook/roles'
        sh 'cp -r ./common/common-centos7.9/playbook/roles/python3 ./aggregate/redis-cluster/playbook/roles'
        sh 'cp -r ./common/common-centos7.9/playbook/roles/docker ./aggregate/redis-cluster/playbook/roles'

    }
    stage('install-redis-cluster') {
        ansible.inside("") {
            sh "cd aggregate/redis-cluster && ANSIBLE_HOST_KEY_CHECKING=false " +
                    "ansible-playbook playbook/install-redis-cluster-from-scratch.yaml -i redis-cluster-hosts -e env_hosts=server1"
        }
    }
}
