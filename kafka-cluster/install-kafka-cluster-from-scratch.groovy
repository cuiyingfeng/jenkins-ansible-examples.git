def ansible = docker.image('pi4k8s/ansible:centos7')
node {

    stage('git chekout') {
        parallel(
                'common': {
                    dir('common'){
                        git branch: 'master', url: 'https://gitee.com/pi4k8s/ansible-exmaples.git '
                    }
                },
                'halo': {
                    dir('kafka-cluster'){
                        git branch: 'master', url: 'https://gitee.com/cuiyingfeng/jenkins-ansible-examples.git'
                    }

                }
        )
    }
    stage('aggregate') {
        sh 'rm -rf aggregate && mkdir aggregate'
        sh 'cp -r ./kafka-cluster/kafka-cluster ./aggregate'
        sh 'cp -r ./common/base-centos7.9/playbook/roles/os-enforce ./aggregate/kafka-cluster/playbook/roles'
        sh 'cp -r ./common/common-centos7.9/playbook/roles/python3 ./aggregate/kafka-cluster/playbook/roles'
        sh 'cp -r ./common/common-centos7.9/playbook/roles/docker ./aggregate/kafka-cluster/playbook/roles'

    }
    stage('install-kafka-cluster') {
        ansible.inside("") {
            sh "cd aggregate/kafka-cluster && ANSIBLE_HOST_KEY_CHECKING=false " +
                    "ansible-playbook playbook/install-kafka-cluster-from-scratch.yaml -i kafka-cluster-hosts -e env_hosts=server1"
        }
    }
}
